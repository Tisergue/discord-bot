const Discord   = require("discord.js")
const client    = new Discord.Client()
const token     = require("./config/token.js")

const Expressions = require("./classes/Expressions.js")
const Statr6      = require("./classes/StatR6.js")

function isDM(channelType, msg) {
    if (channelType !== 'dm') {
        msg.delete().catch(e => console.log(e))
    }
}

let stopInterval = undefined

client.once('ready', () => {
  console.log(`Connecté en tant que: ${client.user.tag}!\n`)
})

client.on('message', msg => {
    const   channelType = msg.channel.type
    const   username    = msg.author.username
    const   userMsg     = msg.content.toLocaleLowerCase()

    if (userMsg.startsWith('/help')) {
        const embed = new Discord.RichEmbed()
            .setTitle('Liste des commandes de MerleApp')
            // .setColor('00AE86')
            // .setDescription('Toutes les commandes doivent commencer par le préfixe /')
            .setFooter('\n\nPour toutes questions, contactez moi.')
            // .setImage("http://i.imgur.com/yVpymuV.png")
            // .setThumbnail("http://i.imgur.com/p2qNFag.png")
            // .setTimestamp()
            // .setURL("https://discord.js.org/#/docs/main/indev/class/RichEmbed")
            .addField('@Les commandes liés aux expressions:', '------------------------------')
            .addField('/add expression', 'Ajoute une expression.')
            .addField('/liste (facultatif: pseudo)', 'Affiche la liste de toutes les expressions ajoutées.')
            .addField('/random', 'Affiche une expression aléatoire.')
            .addField('@La commande pour les statistiques R6:', '------------------------------')
            .addField('/statr6 pseudo (facultatif: plateforme)', 
                        'Recherche les statistiques d\'un joueur.\nPlateforme: ps4, xone et uplay')
            // .addBlankField(true)

        msg.channel.send({embed})
        isDM(channelType, msg)
    }

// ==================================================================================================
// Commandes liés aux expressions
    const Expression = new Expressions(userMsg, username)

    if (userMsg.startsWith('/random')) {
        let randExp = Expression.randomExp

        msg.channel.send(randExp).catch(err => console.log(err))
        isDM(channelType, msg)
    }

    if (userMsg.startsWith('/add')) {
        let expressionToAdd = userMsg.replace('/add', '').trim()

        if (expressionToAdd.length > 5) {
            Expression.add()
            msg.channel.send(`L'expression "${expressionToAdd}" a été ajoutée par @${username}.`)
        } else {
            msg.channel.send('ERREUR: Le champ est vide ou trop court.')
        }
        isDM(channelType, msg) 
    }

    if (userMsg.startsWith('/liste')) {
        let list = Expression.list
        let fields = []
        let max = 25
        let embed = {}
        let searchUser = userMsg.split(' ')
        let username = ''
        let count = 0

        searchUser = searchUser.length > 1 ? searchUser[1].toLowerCase() : ''

        if (searchUser !== '') {
            for (let i=0; i < list.length; i++) {
                userMatch = JSON.parse(list[i]['by']).toLowerCase()

                if (searchUser === userMatch) {
                    fields.push({
                        name: `${JSON.parse(list[i]['expression'])}`,
                        value: `Expression n°${JSON.parse(list[i]['id'])} proposé par @${JSON.parse(list[i]['by'])}`
                    })
                }
            }
            embed = {
                color: 3447003,
                title: `La liste des expressions de beaufs de ${searchUser} ! | ${fields.length} expressions(s)`,
                fields: fields
            }
            msg.channel.send({embed})
        } else {
            if (list.length < 51) {
                for (let i=0; i < list.length; i++) {
                    if (i < max) {
                        fields.push({
                            name: `${JSON.parse(list[i]['expression'])}`,
                            value: `Expression n°${JSON.parse(list[i]['id'])} proposé par @${JSON.parse(list[i]['by'])}`
                        })
                    } else {
                        embed = {
                            color: 3447003,
                            title: `La liste des expressions de beaufs les ptis potes ! | ${max - 24} à ${max} |`,
                            fields: fields
                        }
                        msg.channel.send({embed})
                        fields = []
                        embed = {}
                        fields.push({
                            name: `${JSON.parse(list[i]['expression'])}`,
                            value: `Expression n°${JSON.parse(list[i]['id'])} proposé par @${JSON.parse(list[i]['by'])}`
                        })
                    }

                    if (i === list.length - 1) {
                        embed = {
                            color: 3447003,
                            title: `La liste des expressions de beaufs les ptis potes ! | ${max - 24} à ${i + 1} |`,
                            fields: fields
                        }
                        msg.channel.send({embed})
                    }

                    max = (i === max ? max + 25 : max)
                }
            } else {
                msg.channel.send('Il y a trop d\'expressions de beaufs, laisse moi optimiser ça ;)')
            }
        }
        isDM(channelType, msg) 
    }

    if (userMsg.startsWith('/loop')) {
        let loopInterval    = userMsg.split(' ')
        let defaultInterval = 3600000

        isDM(channelType, msg)

        if (loopInterval.length > 1) {
            time = parseInt(loopInterval[1])
            loopInterval = !isNaN(time) ? (time * 60000) : defaultInterval
            loopInterval = loopInterval < 2147483647 ? loopInterval : defaultInterval
        } else {
            loopInterval = defaultInterval
        }

        console.log(loopInterval)

        stopInterval = setInterval(function() {
            msg.channel.send(Expression.randomExp)
                .then(resp => msg.channel.send('Et une expression au pif, une !'))
                .catch(err => console.log('/loop\n', err))
        }, loopInterval)
        console.log(`La boucle a été activé pour ${loopInterval / 60000} minutes.`)
    }

    if (userMsg.startsWith('/stoploop')) {
        isDM(channelType, msg)  

        if (stopInterval === undefined) {
            console.log('la boucle n\' a pas été lancé')
        } else {
            clearInterval(stopInterval)
            stopInterval = undefined
            console.log('La boucle a été désactivé')
        }
    }
// ==================================================================================================

    if (userMsg.startsWith('/statr6')) {
        let user = userMsg.split(' ')

        user = user.length > 1 ? user[1] : ''
        isDM(channelType, msg) 

        async function load(user, msg) {
            msg.channel.send(`Les stats de ${user} sont en cours de transmission.`, 
            {
                file: 'http://www.mytreedb.com/uploads/mytreedb/loader/ajax_loader_blue_32.gif'
            }).then(res => {
                let stat = new Statr6(msg.content)
                let getStat = stat.stat

                getStat.then(resStat => {
                    if (msg.channel.type !== 'dm' && res) {
                        res.delete()
                    }
                    msg.channel.send(resStat).catch(e => console.log('Erreur affichage statr6 embed', e))
                }).catch(e => console.log('getStat crash:', e))
            })
        }

        load(user, msg)
    }
})

client.login(token.token)