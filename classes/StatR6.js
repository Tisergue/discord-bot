const RainbowSixApi = require('rainbowsix-api-node')
const R6 = new RainbowSixApi()

// @return: un temps en milliseconde en heure.
function msToTime(duration) {
    let hrs = (duration / 60) / 60
    return hrs.toFixed(2)
}

class StatR6 {
    constructor(user) {
        this.user = user
    }

    get stat() {
        let argv = this.user.split(' ')

        if (argv.length > 1) {
            let user = argv[1]
            user = user.trim()
            argv = argv.slice(2)

            let defaultPlatform = 'ps4'
            let DefaultEmbedColor = 3447003

            let platform = argv.find(elem => (elem === 'ps4' || elem === 'xone' || elem === 'uplay'))
            platform = platform === undefined ? defaultPlatform : platform

            if (platform === 'uplay' || platform === 'xone') {
                DefaultEmbedColor = platform === 'xone' ? 2067276 : 9807270
            }
            
            let res = R6.stats(user, platform)
                .then(response => {
                    const r6 = response.player.stats

                    let rankedWins      = r6.ranked.wins
                    let rankedLosses    = r6.ranked.losses
                    let rankedKills     = r6.ranked.kills
                    let rankedDeaths    = r6.ranked.deaths
                    let rankedKD        = r6.ranked.kd
                    let rankedPlayTime  = msToTime(r6.ranked.playtime)
                    let casualWins      = r6.casual.wins
                    let casualLosses    = r6.casual.losses
                    let casualKills     = r6.casual.kills
                    let casualDeaths    = r6.casual.deaths
                    let casualKD        = r6.casual.kd
                    let casualPlayTime  = msToTime(r6.casual.playtime)
                    let totalPT         = msToTime(r6.ranked.playtime + r6.casual.playtime)

                    let embed = {
                        embed: {
                            color: DefaultEmbedColor,
                            author: {
                            name: `Joueur: ${user} | plateforme: ${platform.toUpperCase()}`
                            },
                            fields: [
                                { name: 'Matchs gagnés',value: `classée: ${rankedWins}\ncasual: ${casualWins}\ntotal: ${rankedWins + casualWins}` },
                                { name: 'Matchs perdus', value: `classée: ${rankedLosses}\ncasual: ${casualLosses}\ntotal: ${rankedLosses + casualLosses}` },
                                { name: 'Kills', value: `classée: ${rankedKills}\ncasual: ${casualKills}\ntotal: ${rankedKills + casualKills}` },
                                { name: 'Morts', value: `classée: ${rankedDeaths}\ncasual: ${casualDeaths}\ntotal: ${rankedDeaths + casualDeaths}` },
                                { name: 'Ratio', value: `classée: ${rankedKD}\ncasual: ${casualKD}` },
                                { name: 'Temps de jeu', value: `classée: ${rankedPlayTime} heures\ncasual: ${casualPlayTime} heures\ntotal: ${totalPT} heures` }
                            ]
                        }
                    }

                    return embed
                })
                .catch(error => {
                    console.error(error)
                })
            
            return res.then((res => { return res }))
        } else {
            let errorMsg = `Hep hep hep ! Il semble que tu es oublié de mettre un pseudo ;)`

            return errorMsg
        }
    }
}

module.exports = StatR6