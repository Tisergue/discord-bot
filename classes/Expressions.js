const fs = require('fs')

// Constante
const PATH = './files/expressions.json'

function IsJsonString(str) {
    try {
        JSON.parse(str)
    } catch (e) {
        return false
    }
    return true
}

class Expressions {
    constructor(output, user) {
        this.output = output
        this.user   = user
    }   

    add() {
        const content = JSON.stringify(this.output)
        let exp = content.replace('/add ', '')
        let user = `\"${this.user}\"`

        exp = '"' + exp.charAt(1).toUpperCase() + exp.slice(2)
        fs.readFile(PATH, function (err, data) {
            if (IsJsonString(data) === true) {
                let json = JSON.parse(data)
                let id = json.length + 1
                let newExp = {id: id, expression: exp, by: user}

                json.push(newExp)    
                fs.writeFile(PATH, JSON.stringify(json), function(err){
                if (err) throw err
                console.log('The "data to append" was appended to file!')
                })
            } else {
                console.log('bad json format')
            }
        })
    }

    get randomExp() {
        let file = fs.readFileSync(PATH)

        if (IsJsonString(file) === true) {
            let json = JSON.parse(file)
            let randId = Math.floor(Math.random() * Math.floor(json.length))
            let exp = JSON.parse(json[randId]['expression'])
            let by = JSON.parse(json[randId]['by'])
            let tab = [by, exp]

            return {
                embed: {
                    color: 3447003,
                    description: tab[1],
                    footer: {
                        text: 'Proposé par ' + tab[0]
                    }
                }
            }
        } else {
            console.log('/random get an error')
        }
    }

    get list() {
        let file = fs.readFileSync(PATH)
        
        if (IsJsonString(file) === true) {
            return JSON.parse(file)   
        } else {
            console.log('error')
        }
    }
}

module.exports = Expressions